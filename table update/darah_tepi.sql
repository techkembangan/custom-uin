/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100425
 Source Host           : localhost:3306
 Source Schema         : mastermaswin

 Target Server Type    : MySQL
 Target Server Version : 100425
 File Encoding         : 65001

 Date: 17/01/2023 01:39:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for darah_tepi
-- ----------------------------
DROP TABLE IF EXISTS `darah_tepi`;
CREATE TABLE `darah_tepi`  (
  `no_rawat` varchar(17) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tgl_periksa` date NOT NULL,
  `jam` time NOT NULL,
  `eritrosit` varchar(700) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `trombosit` varchar(700) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `leukosit` varchar(700) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `saran` varchar(700) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kesan` varchar(700) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kemungkinan` varchar(700) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`no_rawat`, `tgl_periksa`, `jam`) USING BTREE,
  CONSTRAINT `darah_tepi_ibfk_1` FOREIGN KEY (`no_rawat`) REFERENCES `reg_periksa` (`no_rawat`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
